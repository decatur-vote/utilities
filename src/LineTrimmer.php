<?php

namespace DecaturVote\Util;

class LineTrimmer {

    /**
     * Return a block of text with all lines `trim`med. All `\r\n`s are replaced with just `\n`.
     *
     * @param $text the input multi-line text containing duplicates
     * @return string multi-line text with no duplicate lines.
     */
    public function trim_all_lines(string $text): string{
        $lines = str_replace("\r\n", "\n", $text);
        $lines = explode("\n", $text);
        $lines = array_map('trim', $lines);
        $trimmed = implode("\n", $lines);
        return $trimmed;
    }

}
