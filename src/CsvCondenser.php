<?php

namespace DecaturVote\Util;

use League\Csv\Reader;
use League\Csv\Writer;

class CsvCondenser {

    /**
     * the relative path to the file to condense
     */
    protected string $file_name;
    /** the current directory */
    protected string $working_dir;
    /** Abolute path to the file */
    protected string $file_path;
    /** OPTIONAL the cli instance, used for logging */
    protected ?\Tlf\Cli $cli;

    /** Absolute path to output file. Same as input file, but with 'condensed-' added to the file's basename. */
    protected string $output_file;

    /**
     * Construct a consenser
     * @param $file_name the relative path to the file to condense
     * @param $working_dir the current directory
     * @param $cli OPTIONAL the cli instance, used for logging
     */
    public function __construct(string $file_name, string $working_dir, ?\Tlf\Cli $cli = null){

        $this->file_name = $file_name;
        $this->working_dir = $working_dir;
        $this->file_path = $working_dir.'/'.$file_name;

        $basename = basename($this->file_path);
        $dirname = dirname($this->file_path);
        $this->output_file = $dirname.'/condensed-'.$basename;
        $this->cli = $cli;

    }

    /**
     * Perform the condensing.
     * Outputs a file.
     * Will not overwrite an existing file.
     */
    public function condense(){
        $file_name = $this->file_name;
        $file_path = $this->file_path;
        $output_file = $this->output_file;
        $this->log("Condense file '$file_name'", "condense");

        if (!file_exists($file_path)){
            $msg = "File '$file_name' does not exist.";
            echo "\n$msg";
            $this->log($msg, "condense");
            return;
        }

        if (file_exists($output_file)){
            $output_file = basename($output_file);
            $msg = "Output file '$output_file' already exists. Stopping";
            echo "\n$msg";
            $this->log($msg, "condense");
            return;
        }

        $writer = $writer = Writer::createFromPath($output_file, 'w+');

        //load the CSV document from a file path
        $csv = Reader::createFromPath($file_path, 'r');
        $csv->setDelimiter(',');
        $csv->setEnclosure('"');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        $writer->insertOne($header);
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object


        // 1. if new record encoutered, insert active(previous) record
        // 2. If new row is NOT a record, then append to description of active(previous) record
        $record_count = 0;
        $active_record = null;
        foreach ($records as $record){
            if (is_numeric($record['No'])&&is_numeric($record['WOID'])){
                $is_new_record = true;
            } else {
                $is_new_record = false;
            }


            if (!$is_new_record){
                // find mis-placed descriptions & info
                // 1. Column 1 has description
                // 2. Description column has description
                // 3. ActionTaken column has description (of action taken!)
                // 4. Description column and ActionTaken column both have descriptions.

                if (strlen(trim($record['No']))>0){
                    $active_record['Descriptions'] .= "\n".$record['No'];
                }
                if (strlen(trim($record['Descriptions']))>0){
                    $active_record['Descriptions'] .= "\n".$record['Descriptions'];
                }
                if (strlen(trim($record['ActionTaken']))>0){
                    $active_record['ActionTaken'] .= "\n".$record['ActionTaken'];
                }

                if (strlen(trim($record['AssignedTo']))>0){
                    $active_record['AssignedTo'] .= " ".$record['AssignedTo'];
                }

            }
            if ($is_new_record){
                if ($active_record !== null){
                    $writer->insertOne($active_record);
                    $record_count++;
                }
                $active_record = $record;

            }
        }
        $writer->insertOne($active_record);
        $record_count++;

        $msg = "Records written: $record_count";
        $this->log($msg, "condensed");
        echo "\n".$msg;
        // echo $writer->toString();
        // echo $csv->toString(); //returns t

        return;
    }

    /**
     * Mirrors \Tlf\Cli->log(). Does not log if the cli instance (passed to constructor) is null.
     */
    public function log(string $msg, string $log_file){
        if ($this->cli==null)return;
        $this->cli->log($msg, $log_file);
    }
}
