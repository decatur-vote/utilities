<?php

namespace DecaturVote\Util;

class Unique {

    /**
     * Return a block of text with duplicate lines removed. 
     * Simply trims each line then uses array_unique() to filter out duplicates.
     *
     * @param $text the input multi-line text containing duplicates
     * @return string multi-line text with no duplicate lines.
     */
    public function return_unique_lines(string $text): string{
        $lines = str_replace("\r\n", "\n", $text);
        $lines = explode("\n", $text);
        $lines = array_map('trim', $lines);
        $unique = array_unique($lines);
        return implode("\n", $unique);
    }

}
