# Decatur Vote Utilities
A collection of one-off utilities used by Decatur Vote

## Utilities
- Spreadsheet Condenser - I am using [PdfToExcel.com](https://www.pdftoexcel.com/) to convert Digital Maintenance Requests (pdf) to a spreadsheet. The resulting spreadsheet is good, but many of the descriptions span multiple rows. The condenser will merge descriptions into a single row. 

## Spreadsheet Consenser Usage
Simply run with command line. It will create a new .csv file from an input .csv file. Uses [https://csv.thephpleague.com/](https://csv.thephpleague.com/)
```bash
bin/dv-util condense path/to/spreadsheet.csv
```
OUTPUTS `path/to/condensed-spreadsheet.csv`
