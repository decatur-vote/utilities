<?php


require_once(__DIR__.'/../vendor/autoload.php');


$cli = new \Tlf\Cli();
// $cli->load_json_file(__DIR__.'/config.json');
$cli->log_dir = __DIR__.'/../log/';
$cli->load_stdin();
$cli->load_command("main", [$cli, 'help_menu'], "show this help menu");

$cli->load_command('condense',
    function($cli, $args) {
        $file_name = $args['--'][0];
        $condenser = new \DecaturVote\Util\CsvCondenser($file_name, getcwd(), $cli);
        $condenser->condense();
    }
    , "Condense a .csv file. Usage: bin/dv-util condense path/to/spreadsheet.csv; Outputs path/to/condensed-spreadsheet.csv"
);


$cli->load_command('unique',
    function($cli, $args) {
        $file_name = $args['--'][0];
        $condenser = new \DecaturVote\Util\Unique();
        $unique_text = $condenser->return_unique_lines(file_get_contents($file_name));
        echo "\n\n$unique_text\n\n";
    }
    , "Get unique lines from a multi-line block of text. Usage: bin/dv-util unique path/to/file.txt; Prints unique text"
);

$cli->load_command('trim-lines',
    function($cli, $args) {
        $file_name = $args['--'][0];
        $path = getcwd().'/'.$file_name;
        $dir = dirname($path);
        $trimmer = new \DecaturVote\Util\LineTrimmer();
        $trimmed = $trimmer->trim_all_lines(file_get_contents($file_name));

        $ext = '.'.pathinfo($path, PATHINFO_EXTENSION);
        $name_clean = substr($path,0,-(strlen($ext)));
        $path_trimmed = $name_clean.'-trim'.$ext;
        file_put_contents($path_trimmed, $trimmed);

        echo "\n\nTrimmed output to $path_trimmed\n\n";
    },
    "Trim each line in a file. Writes trimmed copy to the same directory. Usage: bin/dv-util trim-lines path/to/file.txt; "
);

$cli->execute();
echo "\n";
